Use Case 3: Deponowanie
=====================

**Aktor podstawowy:** Klient


Główni odbiorcy i oczekiwania względem systemu:
-----------------------------------------------

- Klient: oczekuje możliwości zdeponowania pieniędzy na swoim rachunku bankowym bez konieczności wizyty w banku.  

- Bank: oczekuje stałej łączności z bankomatem. 

Warunki wstępne:
----------------

Klient został zidentyfikowany poprzez prawidłowe wpisanie PINu.

Warunki końcowe:
----------------

Transakcja jest bezpieczna. Stan konta został zaktualizowany. 

Scenariusz główny (ścieżka podstawowa):
---------------------------------------

  1. Klient wybiera opcję deponowania pieniędzy.
  2. Bankomat prosi o wprowadzenie ilości banknotów które chcemy wprowadzić.
  3. Klient wprowadza liczbę banknotów do wpłacenia.
  4. Bankomat stwierdza wystarczającą ilość miejsca w kasetce i prosi o wprowadzanie banknotów.
  5. Klient wprowadza gotówkę.
  6. Bankomat sprawdza wprowadzną kwotę i wyświetla liczbę wprowadzonych banknotów prosząc klienta o potwiedzenie.
  7. Klient potwierdza zgodność.
  8. Bankomat pyta czy wydrukować potwiedzenie.
  9. Klient deklaruje chęć wydruku przez bankomat potwierdzenia.
  10. Bankomat wydaje kartę.
  11. Klient zabiera kartę.
  12. Bankomat drukuje potwierdzenie.
  13. Klient zabiera potwierdzenie i odchodzi.

Rozszerzenia (ścieżki alternatywne):
------------------------------------

 1-9a. System zawiesza się.
		1. Bankomat wydaje kartę oraz drukuje potwierdzenie niepowodzenia transakcji.
		
 1-4a. Klient anuluje transakcje.
		1. Bankomat wydaje kartę

 6-9b. Klient anuluje transakcje.
		1. Bankomat wydaje kartę
		2. Bankomat oddaje wpłaconą kwotę.
		
 4a. Bankomat stwierdza niewystarczającą ilość miejsca w kasetce na wpłaty.
		1. Bankomat wydaje kartę.
		2. Bankomat wyświetla komunikat o braku miejsca.
		
 7a.	Klient stwierdza nieprawidłowość kwoty.
		1. Bankomat wydaje kartę.
		2. Bankomat oddaje wpłaconą gotówkę.
		3. Bankomat drukuje potwierdzenie niepowodzenia transakcji.
		
 9,12,13a. Klient nie deklaruje chęci wydruku potwierdzenia.
		1. Bankomat nie drukuje potwierdzenia

 10a. Bankomat nie wydaje karty.
		1. Klient udaje się do oddziału banku.
		
 11a.Klient nie zabiera karty.
		1. Bankomat wciąga kartę.
		2. W celu odzyskania karty klient udaje się do oddziału banku.

 12a. Brak papieru w drukarce.
		1. Bankomat wyświetla informacje o braku papieru.

		
Wymagania specjalne:
--------------------

  
  - Możliwość interfejsu wielojęzycznego.


Wymagania technologiczne oraz ograniczenia na wprowadzane dane:
---------------------------------------------------------------

  1-4,6-9a Niewielki ekran dotykowy o kontach widzenia uniemożliwiających zobaczenie zawartości ekranu osobom stojącym z boku.

  2,3,7,9a Klawiatura numeryczna T9 wraz z 3 przyciskami: zielonym do zatwierdzenia, żółtym do skasowania zawartości i czerwonym to zakończenia korzystania z bankomatu.

Kwestie otwarte:
----------------

  - ...
