package com.apo.atm.entity;

import javax.persistence.*;
import java.util.List;
import java.util.ArrayList;

@Entity
@SequenceGenerator(name="seq1", initialValue=1000, allocationSize=1)
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq1")
    private long id;

    private float bankBalance = 0;

    private Boolean transactionPossible = false;

    @OneToMany
    private List<Card> cards = new ArrayList();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getBankBalance() {
        return bankBalance;
    }

    public void setBankBalance(float bankBalance) {
        this.bankBalance = bankBalance;
    }

    public Boolean getTransactionPossible() {
        return transactionPossible;
    }

    public void setTransactionPossible(Boolean transactionPossible) {
        this.transactionPossible = transactionPossible;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void addCardDoAccount(Card card){
        getCards().add(card);
    }
}
