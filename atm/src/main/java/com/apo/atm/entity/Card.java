package com.apo.atm.entity;

import javax.persistence.*;

@Entity
@SequenceGenerator(name="seq", initialValue=100, allocationSize=1)
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private long id;

    private int pin;

    @ManyToOne
    private CardType type;

    @ManyToOne
    private Account account;

    public Card() {
    }

    public Card(int pin, CardType type, Account account) {
        this.pin = pin;
        this.type = type;
        this.account = account;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
