package com.apo.atm.service;

import com.apo.atm.entity.Account;

public interface AccountService {
    Account findOne(long id);
    String getMoney(Account account, float value);
    String addMoney(Account account, float value);
    Void doTransfer(Account sender, long recipient, float value);
    Void addPhone(Account account, float value, long phoneNumber);
}
