package com.apo.atm.service;

import com.apo.atm.entity.Card;
import com.apo.atm.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    private CardRepository cardRepository;

    @Override
    public Card findOneCard(long id) {
        return cardRepository.findOneById(id);
    }
}
