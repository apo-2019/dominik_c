package com.apo.atm.service;

import com.apo.atm.entity.Account;
import com.apo.atm.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account findOne(long id) {
        return accountRepository.findOneById(id);
    }

    @Override
    public String getMoney(Account account, float value) {
        if(account.getBankBalance() <=0 || account.getBankBalance() < value){
            System.out.println( "brak środków na koncie");
            return "" ;
        }

        account.setBankBalance(account.getBankBalance() - value);

        accountRepository.save(account);
        System.out.println("Odbierz pieniądze");
        return "";
    }

    @Override
    public String addMoney(Account account, float value) {
        account.setBankBalance(account.getBankBalance() + value);
        System.out.println("wpłacono "  + value + " zl");
        return "";
    }

    @Override
    public Void doTransfer(Account sender, long recipient, float value) {

        if(sender.getBankBalance() <=0 || sender.getBankBalance() < value) {
            System.out.println("brak środków na koncie");
            return null;
        }

        if(!sender.getTransactionPossible()){
            System.out.println("Nie masz aktywowanej możliwosci przelewu w bankomacie");
            return null;
        }

        Account recipientAccount = accountRepository.findOneById(recipient);
        recipientAccount.setBankBalance(recipientAccount.getBankBalance() + value);

        sender.setBankBalance(sender.getBankBalance() - value);

        accountRepository.save(recipientAccount);
        accountRepository.save(sender);

        System.out.println("Przelew wykonano");
        return null;

    }

    @Override
    public Void addPhone(Account account, float value, long phoneNumber) {
        if(account.getBankBalance() <=0 || account.getBankBalance() < value){
            System.out.println( "brak środków na koncie");
            return null ;
        }
        account.setBankBalance(account.getBankBalance() - value);

        System.out.println("Doładowano nr " + phoneNumber);
        return null;
    }
}
