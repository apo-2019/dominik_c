package com.apo.atm.service;

import com.apo.atm.entity.Card;

public interface CardService {
    Card findOneCard(long id);
}
