package com.apo.atm;

import com.apo.atm.entity.Account;
import com.apo.atm.entity.Card;
import com.apo.atm.entity.CardType;
import com.apo.atm.repository.AccountRepository;
import com.apo.atm.repository.CardRepository;
import com.apo.atm.repository.CardTypeRepository;
import com.apo.atm.service.AccountService;
import com.apo.atm.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

@Component
public class ApplicationInit implements CommandLineRunner {

    @Autowired
    private CardService cardService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CardTypeRepository cardTypeRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CardRepository cardRepository;

    Scanner scanner = new Scanner(System.in);

    @Override
    public void run(String... args) throws Exception {
        initDb();

        while(true){

            clearScreen();
            System.out.println("Wprowadź kartę (100-105): \n");
            int cardId = scanner.nextInt();
            Card card = cardService.findOneCard(cardId);
            if(card == null) {
                System.out.println("Niepoprawna karta");
                TimeUnit.SECONDS.sleep(2);
                continue;
            }
            Boolean exit = false;
            System.out.println("Wprowadź pin");
            int pin = scanner.nextInt();
            if(!Integer.valueOf(pin).equals(card.getPin())){
                System.out.println("Niepoprawny pin");
                TimeUnit.SECONDS.sleep(2);
                continue;
            }
            while(!exit){

                generateMenu();
                int choice = scanner.nextInt();
                switchMenu(choice, card);
                if(choice == 6){
                    exit = true;
                }
            }
        }

    }

    public static void clearScreen() {
        for(int i = 0 ; i<20 ; i++){
            System.out.println("\b");
        }
    }

    private void initDb(){
        Account a1 = new Account();
        Account a2 = new Account();
        Account a3 = new Account();
        a1.setTransactionPossible(true);
        accountRepository.save(a1);
        accountRepository.save(a2);
        accountRepository.save(a3);

        CardType cardType = new CardType("DEBIT");
        CardType cardType1 = new CardType("CREDIT");
        CardType cardType2 = new CardType("ATM");
        cardTypeRepository.save(cardType);
        cardTypeRepository.save(cardType1);
        cardTypeRepository.save(cardType2);

        cardRepository.save(new Card(1234, cardType, a1));
        cardRepository.save(new Card(1234, cardType1, a2));
        cardRepository.save(new Card(1234, cardType1, a1));
        cardRepository.save(new Card(1234, cardType2, a3));
        cardRepository.save(new Card(1234, cardType2, a2));

    }

    private void generateMenu(){
        System.out.println("Co chcesz zrobić");
        System.out.println("1. Sprawdź stan konta");
        System.out.println("2. Wypłata gotówki");
        System.out.println("3. Wpłata gotówki");
        System.out.println("4. Wykonaj przelew");
        System.out.println("5. Zakup kodów do telefonii pre-paidowej");
        System.out.println("6. Wyjdź");
    }

    private void switchMenu(int choice, Card card){
        switch (choice){
            case 1: System.out.println(card.getAccount().getBankBalance()); break;
            case 2:
                System.out.println("Jaką kwotę chcesz wypłacić?");
                float value = scanner.nextFloat();
                accountService.getMoney(card.getAccount(), value);
                break;
            case 3:
                System.out.println("Jaką kwotę chcesz wpłacić?");
                float value1 = scanner.nextFloat();
                accountService.addMoney(card.getAccount(), value1);
                break;
            case 4:
                System.out.println("Wpisz konto wpłacającego (1000-1005)");
                long account = scanner.nextLong();
                System.out.println("Wpisz kwote przelewu");
                float value2 = scanner.nextFloat();
                accountService.doTransfer(card.getAccount(), account, value2);
                break;
            case 5:
                System.out.println("Podaj nr telefonu");
                int phoneNumber = scanner.nextInt();
                System.out.println("Podaj kwote doładowania");
                float value3 = scanner.nextFloat();
                accountService.addPhone(card.getAccount(), value3, phoneNumber);
                break;
        }
    }
}
