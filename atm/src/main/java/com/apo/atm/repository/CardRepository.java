package com.apo.atm.repository;

import com.apo.atm.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepository extends JpaRepository<Card, Long> {
    Card findOneById(long id);
}
